FROM php:7.0-cli
LABEL maintainer="Nebojsa Djordjevic <nesh@younify.nl>"

WORKDIR /var/www/html/

RUN curl -sL https://deb.nodesource.com/setup_6.x | bash -

RUN apt-get update \
  && apt-get install -y \
    git \
    cron \
    libfreetype6-dev \
    libicu-dev \
    libjpeg62-turbo-dev \
    libmcrypt-dev \
    libpng12-dev \
    libxslt1-dev \
    nodejs \
    build-essential

RUN docker-php-ext-configure \
  gd --with-freetype-dir=/usr/include/ --with-jpeg-dir=/usr/include/

RUN docker-php-ext-install \
  bcmath \
  gd \
  intl \
  mbstring \
  mcrypt \
  pdo_mysql \
  soap \
  xsl \
  zip \
  opcache

RUN npm install -g eslint htmllint-cli

COPY ini/php.ini /usr/local/etc/php/

RUN curl -sS https://getcomposer.org/installer | \
  php -- --install-dir=/usr/local/bin --filename=composer

CMD ["/bin/false"]